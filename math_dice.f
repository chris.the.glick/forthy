\ math_dice.f
\ Written in Forth from after "MATH DICE" in Ahl, _BASIC Computer Games_
\ Should run on gforth and vfx64 Forth

CR .( Reading math_dice.f )    \ output text w/o compiling

\ include random number generator
include ./random.f             \ words RANDY, CHOOSY, RANDOMIZE; variable RNDVAR
CR .( Reading random.f )

: PREP_RND_GEN ( -- ) RANDY RANDOMIZE DROP ;
: RULES ( -- ) CR
  20 SPACES ." Math Dice" CR
  ." Creative Computing  Morristown, New Jersey" CR
  CR
  ." This program generates successive pictures of two dice." CR
  ." When two dice and an equal sign followed by a question" CR
  ." mark have been printed, type your guess, 'guess' and Return." CR
  ." To conclude the lesson, type '0' (zero) as your answer." CR CR ;
: PRINT_TWO_PIPS CR ." I *   * I" ;
: PRINT_CENTER_PIP CR ." I   *   I";
: PRINT_UP_PIP CR ." I     * I" ;
: PRINT_DN_PIP CR ." I *     I" ;
: PRINT_NO_PIP CR ." I       I" ;
: PRINT_SUM_SIGN CR CR ."     + " ;
: ROLL_DIE ( -- n[1,6] ) 6 CHOOSY 1+ ;

: PRINT_DIE ( -- pips ) CR ROLL_DIE
  DUP 1 = 
  IF PRINT_NO_PIP PRINT_CENTER_PIP PRINT_NO_PIP 
  ELSE DUP 2 = 
  IF PRINT_UP_PIP PRINT_NO_PIP PRINT_DN_PIP 
  ELSE DUP 3 = 
  IF PRINT_UP_PIP PRINT_CENTER_PIP PRINT_DN_PIP 
  ELSE DUP 4 = 
  IF PRINT_TWO_PIPS PRINT_NO_PIP PRINT_TWO_PIPS 
  ELSE DUP 5 = 
  IF PRINT_TWO_PIPS PRINT_CENTER_PIP PRINT_TWO_PIPS 
  ELSE DUP 
  IF PRINT_TWO_PIPS PRINT_TWO_PIPS PRINT_TWO_PIPS 
  THEN THEN THEN THEN THEN THEN 
;

: ROLL_DICE_AND_ASK ( --  totalpips ) PRINT_DIE PRINT_SUM_SIGN PRINT_DIE +
  CR
  CR
  ." = ? "
;

: GUESS ( totalpips guess -- ) DUP 0=
  IF DROP CR ." Thanks for playing!" CR DROP \ clear stack
  ELSE OVER SWAP =
    IF ." Right!" CR CR ." The dice roll again..." DROP ROLL_DICE_AND_ASK
    ELSE ." No, count the spots and try again." CR
  THEN THEN ;

: MATH_DICE PREP_RND_GEN RULES ROLL_DICE_AND_ASK ;

