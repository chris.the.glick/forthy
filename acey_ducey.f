\ acey_ducey.f
\ Written in Forth from after "ACEY DUCEY" in Ahl, _BASIC Computer Games_
\ Should run on gforth and vfx64 Forth

CR .( Reading acey_ducey.f )    \ output text w/o compiling

\ include random number generator
include ./random.f             \ words RANDY, CHOOSY, RANDOMIZE; variable RNDVAR
CR .( Reading random.f )

VARIABLE MY_CASH 2 ALLOT
VARIABLE MY_BET 1 ALLOT

\ Set random number generator and initialize variables MY_CASH and MY_BET
: PREP_RND_GEN ( -- ) RANDY RANDOMIZE DROP
  100 MY_CASH !
  0 MY_BET !
;

: RULES ( -- ) CR
  20 SPACES ." Acey Ducey" CR
  ." Creative Computing  Morristown, New Jersey" CR
  CR
  ." Acey Ducey is played in the following manner. The dealer (PC)" CR
  ." deals two cards face up. You have an option to bet or not bet " CR
  ." depending on whether you think the next card will be between" CR
  ." the first two. You start with $100." CR
  ." If you do not want to bet, input: 0 bet" CR
  ." If you want to quit, input: -1 bet" CR
  ." To conclude the lesson, type '0' (zero) as your answer." CR CR ;

: DRAW_CARD ( -- n[1,13] ) 13 CHOOSY 1+ ;

: PLAY_CARD ( card -- card ) DUP 1 =
  IF SPACE ." Ace" CR
  ELSE DUP 11 = 
  IF SPACE ." Jack" CR
  ELSE DUP 12 = 
  IF SPACE ." Queen" CR
  ELSE DUP 13 =
  IF SPACE ." King" CR
  ELSE DUP SPACE . CR
  THEN THEN THEN THEN
;

: SORT_L_H ( c1 c2 -- lo hi ) 2DUP > IF SWAP THEN ;

: PROMPT ( -- ) ." What is your bet? " ;

: DEAL ( -- lo hi card ) CR DRAW_CARD PLAY_CARD DRAW_CARD PLAY_CARD SORT_L_H 
  DRAW_CARD PROMPT ;

: WIN_BET ( bet -- 1 ) ." You won!" MY_CASH +! 1 ;

: LOSE_BET ( bet -- 0|-1 ) MY_CASH @ SWAP - DUP 1 <
  IF ." You're bust! Game over." -1
  ELSE ." You lost." MY_CASH ! 0
  THEN 
;

: IS_BETWEEN_LIMITS? ( lo hi card -- -1/0 ) DUP ROT < ROT ROT SWAP > AND ;

: CHECK_IF_WIN ( lo hi card bet -- n )
  MY_BET ! \ push bet to MY_BET, now ( lo hi card )
  ROT ROT 2DUP =
  IF
    ROT =
    IF MY_BET @ WIN_BET SWAP DROP CR
    ELSE MY_BET @ LOSE_BET SWAP DROP CR
    THEN 
  ELSE 
    ROT IS_BETWEEN_LIMITS?
    IF MY_BET @ WIN_BET CR
    ELSE MY_BET @ LOSE_BET CR
    THEN
  THEN
;

: WIN_CHECK_RESULT ( n -- empty | lo hi card ) DEPTH 1 =
  IF
    DUP -1 = 
    IF 0 MY_BET ! 0 MY_CASH ! DROP \ zero variables since game is done
    ELSE ." You have $" MY_CASH @ . DROP CR DEAL
    THEN
  THEN
;

: VALID_BET? ( bet -- bet )
  DUP MY_CASH @ >
  IF MY_CASH @ SWAP DROP ." Bet exceeded cash, so using all of it" CR
  THEN
;

: BET ( lo hi card bet -- empty | lo hi card ) 
  VALID_BET?
  SWAP DUP CR ." Card: " PLAY_CARD DROP
  SWAP DUP 0 <
  IF 2DROP CR ." Thanks for playing!" 2DROP CR \ clear stack
  ELSE DUP 0=
    IF DROP CR ." Chicken!!" CR DROP 2DROP DEAL
    ELSE CR CHECK_IF_WIN
  THEN THEN
  \ If stack depth is 1, last bet ended with 1 (win), 0 (lost), -1 (bust)
  WIN_CHECK_RESULT
;

: ACEY_DUCEY PREP_RND_GEN RULES DRAW_CARD DRAW_CARD 2DROP DEAL ;
