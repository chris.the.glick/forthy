\ Gforth-friendly versions of code from "Starting FORTH" by Leo Brodie
\ User beware: I need to confirm this code still works

\ 1. Fundamental FORTH
: STAR 42 emit ;
: MARGIN CR 30 SPACES ;
: STARS 0 do STAR loop ;
: BLIP MARGIN STAR ;
: BAR MARGIN 5 STARS ;
\ Combine the above to output a big 'F'
: F BAR BLIP BAR BLIP BLIP CR ;

: GREET ." Hello, I speak FORTH. " ;
: FOUR-PLUS 4 + . ;

\ 2. How to Get Results
: YARDS>IN 36 * ;
: FT>IN 12 * ;
: INCHES ;
: YARD YARDS ;
: FOOT FEET ;
: INCH ;
: FLIGHT-DISTANCE + * ;
: QUARTERS 4 /MOD . ." ones and " . ." quarters" ;
: CONVICTED-OF 0 ;
: WILL-SERVE . ." years " ;
: HOMICIDE 20 + ;
: ARSON 10 + ;
: BOOKMAKING 2 + ;
: TAX_EVASION 5 + ;


\ 3. The Editor (and Staff)
\ This material does not apply to Gforth

\ 4. Decisions, Decisions...
: ?FULL 12 = if ." It's full" then ;
: ?TOO-HOT 220 > if ." DANGER: Reduce Heat!" then ;
: ?DAY 32 < if ." Looks like a valid day of a month" else ." No way" then ;
: /CHECK dup 0= if ." Division by zero is invalid" 2drop else / then ;
: EGGSIZE dup 18 < if ." Reject" else
          dup 21 < if ." Small" else
          dup 24 < if ." Medium" else
          dup 27 < if ." Large" else
          dup 30 < if ." Extra Large" else
          ." Error"
          then then then then then drop ;
: TEST if ." Non-Zero" else ." Zero" then ;
: /CHECK2 dup if / else ." Division by zero is invalid" drop then ;
: ?DAY2 dup 1 < swap 31 > + 
    if ." Not a valid month day" else ." Valid month day" then ;
: BOXTEST ( length width height -- )
    6 > ROT 22 > ROT 19 > and and if cr ." Box dimensions are big enough" then ;
: /CHECK3 ?dup if / then drop ;
: /CHECK4 dup 0= abort" abort: Zero denominator" / ;
: ENVELOPE /CHECK cr ." The answer is " . ;

\ 5. The Philosophy of Fixed Point
: DIFFERENCE - ABS ;
\ Whether commission is lesser of $50 or 10% of price
: COMMISSION 10 / 50 MIN ;

\ Calculate ax^2 + bx + c for x, see PDF for step-by-step
: QUADRATIC ( a b c x -- n )
    >R SWAP ROT I * + R> * + ;

\ The */ operator is how to get two-thirds of 171: 171 2 3 */
: % ( n1 n2 -- n2% of n1 ) 100 */ ;

\ Calculate "rounded percentage"
: R% ( n1 n2 -- n2% of n1 ) 10 */ 5 + 10 / ;

\ Slicker version for "rounded percentage"
: R% ( n1 n2 -- n2% of n1 ) 50 */ 1+ 2/ ;

\ Calculate area of circle as r^2 * pi
: PI dup * 31416 10000 */ ;
: PI2 dup * 355 113 */ ;

\ 6. Throw It for a loop
: TEST 10 0 do cr ." Howdy" loop ;
: DECADE 10 0 do i . loop ;
: MULTIPLICATIONS ( n -- n[1,10] ) cr 11 1 do dup i * . loop drop ;
: COMPOUND ( amt int -- )
    swap 21 1 cr do ." Year " i . 3 SPACES
    2dup R% + dup ." Balance: " . cr loop 2drop ;

\ Prints 256 stars with a CR every 16th
: RECTANGLE 256 0 do i 16 mod 0= if cr then ." *" loop ;

: POEM cr 11 1 do i . ." little "
    i 3 mod 0= if ." Indians " cr then loop
    ." Indian boys." ;

: TABLE cr 11 1 do i MULTIPLICATIONS loop ;
: TABLE2x2 cr 11 1 do
    11 1 do i j * 5 u.r loop cr loop ;

: PENTAJUMPS 50 0 do i . 5 +loop ;
: FALLING -10 0 do i . -1 +loop ;
: INC-COUNT ( increment to from ) do i . dup +loop drop ;
: DOUBLING 32767 1 do i . i +loop ;
: DOUBLED 6 1000 21 1 do cr
    ." Year " i 2 u.R
    2dup R% + dup ." , Balance: " .
    dup 2000 > if cr cr ." More than doubled in "
        i . ." years." leave then
    loop drop ;

\ 7. A Number of Kinds of Numbers
: PRINTABLES 127 32 do i emit space loop ;
: BINARY 2 base ! ;
: OCTAL 8 base ! ;

\ Other content is generally not supported in Gforth

\ 8. Variables, Constants, and Arrays
VARIABLE DATE
VARIABLE MONTH
VARIALE YEAR
: !DATE ( mon day yr -- )  YEAR ! DATE ! MONTH ! ;
: .DATE ( -- ) MONTH ? DATE ? YEAR ? ;

VARIABLE EGGS
: RESET 0 EGGS ! ;
: EGG 1 EGGS +! ;

220 CONSTANT LIMIT
: ?TOO.HOT LIMIT > if ." DANGER -- Reduce heat!" then ;

\ For */ approximations
355 113 2CONSTANT PIPI
10000 PIPIP */ .

VARIABLE LIMITS 8 ALLOT
220 LIMITS !
\ Store values for burners 0~4
: LIMIT 2* LIMITS + ;
: ?OVERTEMP ( burner# temp -- )
    LIMIT @ > if ." DANGER -- Reduce Heat!" then ;

\ Create array of 12 bytes
VARIABLE COUNTS 10 ALLOT
\ Fill 12 bytes with value of zero
COUNTS 12 0 FILL
: COUNTS_RESET COUNTS 12 ERASE ;
: COUNTER ( grade -- counter_addr ) 2* COUNTS + ;
: TALLY  ( grade -- ) COUNTER 1 SWAP +! ;
: CATEGORY ( weight -- grade )
    dup 18 < if 0 else
    dup 21 < if 1 else
    dup 24 < if 2 else
    dup 27 < if 3 else
    dup 30 < if 4 else 5
    then then then then then
    swap drop ;
: LABEL ( grade -- )
    dup 0= if ." Reject" else
    dup 1 = if ." Small" else
    dup 2 = if ." Medium" else
    dup 3 = if ." Large" else
    dup 4 = if ." Extra Large" else
    ." Error"
    then then then then then drop ;
: EGGSORT ( weight -- ) CATEGORY dup LABEL TALLY ;
: REPORT ( -- ) ." Quantity    Size" cr
    6 0 DO I COUNTER @ 5 U.R
    7 spaces I LABEL cr loop ;

\ Gforth version of the above operation using 64-bit cells
\ Create the 6 cells
create COUNTS 6 cells allot
: COUNTS_RESET ( -- ) 6 0 DO 0 COUNTS I CELLS + ! LOOP ;
: COUNTER ( grade -- counter_addr ) COUNTS swap 8 * + ;
: TALLY ( grade -- ) COUNTER 1 swap +! ;
\ CATEGORY is the same
\ LABEL is the same
\ EGGSORT is the same
: REPORT ( -- ) cr 2 SPACES ." Qty.   Size" cr
    6 0 DO I COUNTS SWAP 8 * + @ 5 U.R
    2 SPACES I LABEL cr 
    loop ;

\ Gforth version using 6 bytes of a cell
create COUNTS_C
: COUNTS_C_RESET counts_c 0 swap ! ;
: COUNTER_C ( grade -- byte_addr ) COUNTS_C swap + ;
: TALLY_C ( grade -- ) COUNTER_C dup c@ 1+ swap c! ;
\ CATEGORY is the same
\ LABEL is the same
: EGGSORT_C CATEGORY dup LABEL TALLY_C ;
: REPORT_C cr 2 SPACES ." Qty.   Size" cr
    6 0 do counts_c i + c@ 5 U.R
    2 SPACES i LABEL cr
    loop ;

\ 9. Under the Hood
\ Pointers to functions, where HELLO's address goes into 'ALOHA
: HELLO ." Hello" ;
: G-BYE ." Good-bye" ;
variable 'ALOHA
: ALOHA 'ALOHA @ EXECUTE ;
\ Assign address for HELLO to 'ALOHA which is executed by ALOHA
' HELLO 'ALOHA !

\ 9.5
VARIABLE 'TO-DO 12 8 * ALLOT ( 96 bytes )
: TO-DO ( index -- addr ) 1- 8 * 'TO-DO + ;

: GREET ." Oi, eu falo FORTH!" ;
: SEQUENCE 11 1 do i . loop ;
: TILE 10 5 BOX ;
: NOTHING ;

' GREET    1 TO-DO !
' SEQUENCE 2 TO-DO !
' TILE     3 TO-DO !
' NOTHING  4 TO-DO ! 
' NOTHING  5 TO-DO ! 
' NOTHING  6 TO-DO ! 

\ Take index from stack, x8 to get byte-clump in 'TO-DO, get addr and execute it
: DO-SOMETHING ( index -- ) TO-DO @ EXECUTE ;


\ 10. I/O and You

\ Random number generator
\ where CHOOSE returns random int with range 0 = or < u2 < u1
\ Example: 10 CHOOSE
VARIABLE RND HERE RND !
: RANDOM RND @ 31421 * 6927 + DUP RND ! ;
: CHOOSE ( u1 -- u2 ) RANDOM um* SWAP DROP ;

\ Fill pad with 1024 'A' characters
PAD 1024 65 FILL

\ Fill pad with 1024 space characters
PAD 1024 32 FILL

