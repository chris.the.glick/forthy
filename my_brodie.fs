\ simple greeting
: GREET ( -- ) ." Oi, eu falo Forth." ;

\ n lo hi reports whether n is between lo and hi
: BETWIXT ( n lo-limit hi-limit -- ) ROT DUP ROT < ROT ROT SWAP >= AND IF TRUE . ELSE FALSE . THEN ;

\ n SIGN.TEST returns with value of sign
: SIGN.TEST ( n -- ) DUP 0> IF ." Positivo " ELSE DUP 0< IF ." Negativo " ELSE ." Zero " THEN THEN DROP ;

\ put target on stack then n GUESS to guess until correct
: GUESS ( n guess -- n | blank) SWAP DUP ROT SWAP 2DUP > IF ." too high " DROP DROP ELSE < IF ." too low " ELSE ." Correct!" DROP THEN THEN ;

\ If -4 <= n <= 4, spells value
: SPELLER ( n -- ) DUP ABS 4 > IF ." Out of range " DROP ELSE DUP 0< IF ." negative " THEN THEN DUP ABS 4 = IF ." four " ELSE DUP ABS 3 = IF ." three " ELSE DUP ABS 2 = IF ." two " ELSE DUP ABS 1 = IF ." one " ELSE ." zero " THEN THEN THEN THEN DROP ;

\ reports whether n between lo and hi, leaves n
: BETWIXT2 ( n lo-limit hi-limit -- n ) ROT DUP ROT < ROT ROT DUP ROT >= SWAP ROT ROT AND IF ." Between " ELSE ." Not Between " THEN ;

\ check whether stack is empty, report size if not
: STACKCHECK depth dup 0= IF ." empty stack" ELSE ." size: " . THEN ;

\ guessing whether n is between lo and hi limits
: TRAP ( n lo-limit hi-limit -- n )
  -rot 2dup = 2swap 2dup = -rot 2swap and
  IF     .\" You got it! " drop drop
  ELSE   -rot -rot 2dup = 2swap 2dup = -rot 2swap or
         IF     .\" You hit a limit " -rot
         ELSE   rot rot BETWIXT2
         THEN
  THEN ;

\ Chapter 1. Fundamental Forth
: STAR 42 EMIT ;            \ output '*'
: CHAR_STAR [CHAR] * EMIT ; \ output '*' 
: MARGIN CR 30 SPACES ;
: BLIP MARGIN STAR ;
: BAR MARGIN 5 STAR ;
: F BAR BLIP BAR BLIP BLIP CR ;
: GREET ." Hello, I speak Forth " ;
: FOUR-MORE 4 + ;           \ adds 4 to whatever was on the stack

\ Chapter 2. How to Get Results
: YARDS>IN 36 * ;
: FT>IN 12 * ;
: YARDS ( yds -- in ) 36 * ;
: QUARTERS 4 /MOD . ." ones and " . ." quarters " ;

\ 3. The Editor (and Staff)

\ 4. Decisions, Decisions
: ?TOO-HOT  220 > IF ." Danger -- reduce heat " THEN ;
: ?DAY  32 < IF  ." Looks good " ELSE  ." no way " THEN ;
: /CHECK   
	  DUP 0= IF  ." invalid " DROP  
	       ELSE /
          ( add your statement here ) 
	       THEN ;
: EGGSIZE	DUP  18 < IF  ." reject "      ELSE
			DUP  21 < IF  ." small "       ELSE
			DUP  24 < IF  ." medium "      ELSE
			DUP  27 < IF  ." large "       ELSE
			DUP  30 < IF  ." extra large " ELSE
				      ." error "
			THEN THEN THEN THEN THEN DROP ;	
: /CHECK  DUP IF  ( your statement here )  ELSE  ." invalid " DROP  THEN ;
: VEGETABLE  DUP 0<  SWAP 10 MOD 0= +
		IF  ." ARTICHOKE "  THEN ;
: ?DAY  DUP 1 <  SWAP 31 > +
		IF ." No way " ELSE ." Looks good " THEN ;
: BOXTEST ( length width height -- )
		6 >  ROT 22 >  ROT 19 >  AND AND 
		IF ." Big enough " THEN ;
: /CHECK  DUP 0= ABORT" zero denominator " / ;
: ENVELOPE  /CHECK ." The answer is " . ;

\ Chapter 5, The Philosophy of Fixed Point: 
: DIFFERENCE   - ABS ;
: COMMISSION   10 /  50 MIN ;
: QUADRATIC  ( a b c x -- n )
		>R SWAP ROT R@ *  + R> *  + ;
: R%  10 */  5 +  10 / ;        \ rounded percent
: PI  DUP * 355 113 */ ;

\ 5-1. given ( a b c -- ) return -a*b/c
: ALG ( a b c -- ) ROT NEGATE * / . ;

\ book answer is shorter
: -ab/c  ( a b c -- -ab/c )  */ NEGATE ;

\ 5-2. return max from ( a b c d -- )
: 4MAX ( a b c d -- max ) MAX -ROT MAX MAX . ;

\ book answer is shorter
: LARGEST ( a b c d -- largest ) MAX MAX MAX  . ;

\ 5-3. skipped, as it is preparation for 4

\ 5-4. temperature conversions follow ( n -- )
: F>C ( n -- ) 32 - 10 18 */ . ;
: F>K ( n -- ) 32 - 10 18 */ 273 + . ;
: C>F ( n -- ) 18 10 */ 32 + . ;
: C>K ( n -- ) 273 + . ;
: K>F ( n -- ) 273 - 18 10 */ 32 + . ;
: K>C ( n -- ) 273 - . ;

\ book answer leaves values on the stack to recycle words, which I considered
: F>C  ( fahr -- cels )  32 -  10 18 */ ;
: C>F  ( cels -- fahr )  18 10 */  32 + ;
: C>K  ( cels -- kelv )  273 + ;
: K>C  ( kelv -- cels )  273 - ;
: F>K  ( fahr -- kelv )  F>C C>K ;
: K>F  ( kelv -- fahr )  K>C C>F ;


\ Chapter 6, Throw It for a Loop
: TEST   10 0 DO  CR ." Hello "  LOOP ;
: DECADE  10 0 DO  I .  LOOP ;
: SAMPLE  -243 -250 DO  I .  LOOP ;
: MULTIPLICATIONS  CR 11 1 DO  DUP I * .  LOOP  DROP ;
: COMPOUND  ( amt int -- )
  CR SWAP 21 1 DO  ." YEAR " I . 3 SPACES
    2DUP R% +  DUP ." BALANCE " . CR 
  LOOP  2DROP ;
: RECTANGLE  256 0 DO   I 16 MOD 0= IF  CR  THEN
    ." *"
  LOOP ;
: POEM  CR 11 1 DO  I . ." Little " 
  I 3 MOD 0= IF ." indians " CR THEN  
  LOOP
  ." indian boys. " ;
: TABLE  CR 11 1 DO   
  11 1 DO  I J *  5 U.R  LOOP
  CR LOOP ;
: PENTAJUMPS  50 0 DO  I .  5 +LOOP ;
: FALLING  -10 0 DO  I .  -1 +LOOP ;
: INC-COUNT  DO  I . DUP +LOOP  DROP ;
: TEST  100 10 DO  I . -1 +LOOP ;
: FIVES  100 0 DO  I 5 * . LOOP ;
: DOUBLED
  6 1000 21 1 DO  CR ." YEAR " I 2 U.R
    2DUP R% +  DUP ."    BALANCE " .
    DUP 2000 > IF
      CR CR ." more than doubled in " I . ." years " LEAVE  
    THEN
  LOOP 2DROP ;
: RECTANGLE  PAGE 256 0 DO  I 16 MOD 0= IF  CR  THEN  ." *"  LOOP ;

\ 6.1 print n starts on the same line
: NSTARS ( n -- ) 0 DO [CHAR] * EMIT LOOP ;

\ 6.2 print out box w stars wide and h rows high
: BOX ( w h -- ) CR 
  0 DO
    DUP NSTARS CR \ ( n -- )
  LOOP DROP ;

\ 6.3 create a rhomboid, each row 10 stars wide, grows down and right
: RHOMBOID ( h -- ) CR
  0 DO I SPACES
    10 NSTARS CR
  LOOP ;

\ 6.4 create rhomboid that grows down and left
: /RHOMBOID ( h -- ) CR
  1 - 0 SWAP DO I SPACES
    10 NSTARS CR
  -1 +LOOP ;

\ 6.5 do 6.4 using BEGIN...UNTIL
: /RHOMBEGIN ( h -- ) CR
  BEGIN 1- DUP SPACES
    10 NSTARS CR DUP 0=
  UNTIL ;

\ 6.6 prints a suggested number of diamonds
\ I made TOP and BOT words to print the triangles
: TOP 11 1 DO CR I DUP 1 = IF 11 I - SPACES NSTARS CR ELSE 11 I - SPACES I DUP 1 - + NSTARS DROP CR THEN LOOP ;
: BOT 11 1 DO CR I DUP 10 = IF I SPACES 1 NSTARS DROP ELSE I SPACES 21 I 2 * - NSTARS DROP THEN LOOP ;
: DIAMONDS ( n -- ) 0 DO TOP BOT LOOP CR ;

\ 6.6 Book answer is far more concise and relies on I always being available
: STAR   [CHAR] * EMIT ;
: STARS  ( #stars -- )  0 ?DO  STAR  LOOP ;
: TRIANGLE  ( increment limit start -- )
   DO  CR  9 I - SPACES  I 2* 1+ STARS  DUP +LOOP  DROP ;
: DIAMONDS  ( #diamonds -- )
   0 ?DO  1 10 0 TRIANGLE  -1 0 9 TRIANGLE  LOOP ;

\ 6.7 new variation on interest calculation that uses LEAVE
: R%  ( n percentage -- rounded_val ) 10 */  5 +  10 / ;
: NDOUBLED ( interest start_bal -- ) 
    DEPTH 2 = IF
      21 1 DO CR
      ." YEAR " I 3 U.R 2DUP R% + DUP ."   BAL: " .
      DUP 2000 > IF CR CR ." more than doubled in " I . ." years " LEAVE THEN
      LOOP 2DROP
    ELSE 
      ." Need interest and starting balance on stack " CR ." Currently " DEPTH . ." elements on stack."
    THEN ;

 \ 6.8 ** as exponential power function but fails with power = 0
 : ** ( n power -- n**power ) 0 DO I 0= IF 1 OVER * ELSE  OVER * THEN LOOP NIP ;

\ 6.8 Book answer
: ** ( num pow -- n1**n2 ) 1 SWAP  ?DUP IF  0 DO  OVER * LOOP  THEN  NIP ;

\ 8.1a Bake-pie and Eat-pie 
variable PIES 0 ,
: BAKE-PIE ( -- ) PIES 1 swap +! ;
: EAT-PIE ( -- ) PIES @
  0= if cr ." What pie?" cr drop 
  else PIES -1 swap +! ." Thanks for the pie!" cr
  then ;

\ 8.1b Move pies into the freezer
variable FREEZER 0 ,
: FREEZE-PIES ( -- ) PIES @ FREEZER +! 
  0 PIES ! ;

\ 8.2 display the base; e.g., HEX .BASE 15 ok
: .BASE ( -- )  base @ dup decimal . base ! ;

\ 8.3 .M, which doesn't seem to work
variable places 0 ,
: M. swap over dabs <#
  PLACES @ ?dup if 0 do # loop 46 hold then
  #s sign #> type space ;

\ 8.4 Working with pencils, but doing as bytes in a cell
variable PENCIL_BOX 0 ,
0 constant RED
1 constant BLUE
2 constant GREEN
3 constant ORANGE
: PENCILS ( n byte -- | byte -- addr ) depth 
  2 = if PENCIL_BOX + c!
  else PENCIL_BOX + 
  then ;
23 RED PENCILS
15 BLUE PENCILS
12 GREEN PENCILS
0 ORANGE PENCILS
BLUE PENCILS ?

\ 8.5 Histogram of stars
create HISTO 70 c, 13 c, 1 c, 0 c, 52 c, 63 c, 22 c, 20 c, 9 c, 10 c,
: RESET_HISTO 10 0 do i HISTO + 0 swap c! loop ;
: STARS ( max -- ) ?dup if 0 do [char] * emit loop then ;
: PLOT ( -- ) cr 
  10 0 do i HISTO + c@ dup 2 u.R 1 spaces STARS cr loop ;
: VI HISTO 10 chars dump ;

\ 8.6 tic tac toe
create BD 9 chars allot
: CLR_BD ( -- ) 9 0 do i BD + 0 swap c! loop ;
: BV BD 9 chars dump ;
: VDIV 1 spaces ." |" 1 spaces ;
: HDIV ." ---------" ;
: FILL_BOX ( u -- x_o ) BD + c@ 
  dup 0= if 1 spaces else 
  dup 1 = if ." X" else
  ." O" then then drop ;

: DRAW_BD ( -- ) cr
  0 FILL_BOX VDIV 1 FILL_BOX VDIV 2 FILL_BOX cr HDIV cr 
  3 FILL_BOX VDIV 4 FILL_BOX VDIV 5 FILL_BOX cr HDIV cr 
  6 FILL_BOX VDIV 7 FILL_BOX VDIV 8 FILL_BOX cr ;

: X! ( pos -- ) abs dup 9 >
  if ." Enter position 1-9, please. " cr
  else 1- BD + dup c@ 0=
    if 1 swap c!
    else c@ 1 = 
      if ." X already here"
      else ." Y already here"
      then
    then
  then DRAW_BD ;

: Y! ( pos -- ) abs dup 9 >
  if ." Enter position 1-9, please. " cr
  else 1- BD + dup c@ 0=
    if -1 swap c!
    else c@ 1 = 
      if ." X already here"
      else ." Y already here"
      then
    then
  then DRAW_BD ;

\ 9 Under the Hood
: HELLO ." Hi there! " ;
: G-BYE ." Adios. " ;
VARIABLE 'ALOHA 
: ALOHA 'ALOHA @ EXECUTE ; \ execute the address of the word stored in 'ALOHA

\ 9.5
VARIABLE 'TO-DO 12 8 * ALLOT ( 96 bytes )
: TO-DO ( index -- addr ) 1- 8 * 'TO-DO + ;

: GREET ." Oi, eu falo FORTH!" ;
: SEQUENCE 11 1 do i . loop ;
: TILE 10 5 BOX ;
: NOTHING ;

' GREET    1 TO-DO !
' SEQUENCE 2 TO-DO !
' TILE     3 TO-DO !
' NOTHING  4 TO-DO ! 
' NOTHING  5 TO-DO ! 
' NOTHING  6 TO-DO ! 

\ Take index from stack, x8 to get byte-clump in 'TO-DO, get addr and execute it
: DO-SOMETHING ( index -- ) TO-DO @ EXECUTE ;

\ 10 I/O and You

\ Random number generator
\ where CHOOSE returns random int with range 0 = or < u2 < u1
\ Example: 10 CHOOSE
VARIABLE RND HERE RND !
: RANDOM RND @ 31421 * 6927 + DUP RND ! ;
: CHOOSE ( u1 -- u2 ) RANDOM um* SWAP DROP ;

\ Using EXPECT to put input to a variable then print it back out
\ Create 80-byte variable
VARIABLE DABUFF 80 ALLOT

\ Expect up to 80 chars, or the first CR, of input.
\ EXPECT ( addr u -- )
DABUFF 80 EXPECT
\ < enter some text > then play back 60 chars
' DABUFF 3 + 60 TYPE 

